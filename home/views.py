from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'home/home.html')

def skills(request):
    return render(request, 'home/skills.html')